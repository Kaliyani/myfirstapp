import React, { Component } from 'react';
import './Input.css'
class Input extends Component {
    render() {
        const {inputChange, buttonClick, currentValue} = this.props
        return (
            <div className='body'>
                <input className='Input-field'
                    value={currentValue.text}
                    onChange={inputChange}
                />
                <button className='Button-style' onClick={buttonClick}>Add</button>
            </div>
        );
    }
}

export default Input;