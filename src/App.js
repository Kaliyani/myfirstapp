import React, { Component } from 'react';
import './App.css';
import Input from './Input.js';
import ListItem from './ListItem.js';

class App extends Component {
  constructor(){
    super()
    this.state = {
      arrayList:[],
      listItem:{text:'', key:''}
    }
    this.handleInputChange = this.handleInputChange.bind(this)
    this.onSumbitClick = this.onSumbitClick.bind(this)
    this.onCancelClick = this.onCancelClick.bind(this)
  }

  handleInputChange(e){
    let itemValue = e.target.value
    let listItem = {text:itemValue, key:Math.random()}
    //this.setState({...this.state, listItem: listItemValue})
    if(itemValue !== ''){
      this.setState({...this.state, listItem: listItem})
    }
    
  }

  onSumbitClick(e){
    e.preventDefault();
    let newItemValue = this.state.listItem;
    if(newItemValue.text !==''){
      const items = [...this.state.arrayList, newItemValue]
      this.setState({arrayList:items, listItem:{text:'',key:''}})
    }
  }

  onCancelClick(key){
    const removeItem = this.state.arrayList.filter( item => {
      return item.key !== key
    })
    this.setState({...this.state, arrayList: removeItem})
  }
  render() {
    const {arrayList, listItem} = this.state;
    return (
      <div className="App">
        <header>
          <h1>Todo App</h1>
        </header>
        <section className='content'>
          <Input 
              inputChange={this.handleInputChange}
              buttonClick={this.onSumbitClick}
              currentValue={listItem}
            />
          <section className='List-Content'>
            {
              arrayList.map(list => <ListItem key={Math.random()} data={list} deleteItem={this.onCancelClick} />)
            }
          </section>
          <footer className='footer'>
            Total List Item {arrayList.length}
          </footer>
        </section>
        
      </div>
    );
  }
}

export default App;
