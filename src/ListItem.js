import React, { Component } from 'react';
import cancel from './cancel.svg';
import './ListItem.css'
class ListItem extends Component {
    constructor(){
        super()
        this.callDeleteFunction = this.callDeleteFunction.bind(this)
    }
    callDeleteFunction(){
        this.props.deleteItem(this.props.data.key)
    }
    render() {
        const {text} = this.props.data;
        return (
            <div className='body'>
                <div className='item'>{text}</div>
                <img className='btn' src={cancel} alt="cancel" onClick={this.callDeleteFunction}/> 
            </div>
        );
    }
}

export default ListItem;